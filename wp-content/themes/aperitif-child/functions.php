<?php

if ( ! function_exists( 'aperitif_child_theme_enqueue_scripts' ) ) {
	/**
	 * Function that enqueue theme's child style
	 */
	function aperitif_child_theme_enqueue_scripts() {
		$main_style = 'aperitif-main';
		
		wp_enqueue_style( 'aperitif-child-style', get_stylesheet_directory_uri() . '/style.css', array( $main_style ) );
	}
	
	add_action( 'wp_enqueue_scripts', 'aperitif_child_theme_enqueue_scripts' );
}