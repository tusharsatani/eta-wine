<?php // Silence is golden.
// Control core classes for avoid errors
if (class_exists('CSF')) {

    $prefix = '_h5vp_';

//
    // Create a settings page.
    CSF::createMetabox($prefix, array(
        'title' => 'Configure Your Video Player',
        'post_type' => 'videoplayer',
        'data_type' => 'unserialize',
    ));

    //
    // Create a section
    CSF::createSection($prefix, array(
        'title' => '',
        'fields' => array(
            array(
                'id' => 'h5vp_video_link',
                'type' => 'upload',
                'title' => 'Source URL',
                'placeholder' => 'https://',
                'library' => 'video',
                'button_title' => 'Add Video',
                'attributes' => array('class' => 'h5vp_video_link'),
                'desc' => 'select an mp4 or ogg video file. or paste a external video file link',
            ),
            array(
                'id' => 'h5vp_video_thumbnails',
                'type' => 'upload',
                'title' => 'Video Thumbnail',
                'library' => 'image',
                'button_title' => 'Add Image',
                'placeholder' => 'https://',
                'attributes' => array('class' => 'h5vp_video_thumbnails'),
                'desc' => 'specifies an image to be shown while the video is downloading or until the user hits the play button',
            ),
            array(
                'id' => 'h5vp_repeat_playerio',
                'type' => 'button_set',
                'title' => 'Repeat',
                'options' => array(
                    'once' => 'Once',
                    'loop' => 'Loop',
                ),
                'default' => 'once',
            ),
            array(
                'id' => 'h5vp_muted_playerio',
                'type' => 'switcher',
                'title' => 'Muted',
                'desc' => 'On if you want the video output should be muted',
                'default' => '0',
            ),
            array(
                'id' => 'h5vp_auto_play_playerio',
                'type' => 'switcher',
                'title' => 'Auto Play',
                'desc' => 'Turn On if you  want video will start playing as soon as it is ready. <a href="https://developers.google.com/web/updates/2017/09/autoplay-policy-changes">autoplay policy</a>',
                'default' => '',
            ),
            array(
                'id' => 'h5vp_player_width_playerio',
                'type' => 'spinner',
                'title' => 'Player Width',
                'unit' => 'px',
                'max' => '5000',
                'min' => '200',
                'step' => '50',
                'desc' => 'set the player width. Height will be calculate base on the value. Leave blank for Responsive player',
                'default' => '',
            ),
            array(
                'id' => 'h5vp_auto_hide_control_playerio',
                'type' => 'switcher',
                'title' => 'Auto Hide Control',
                'desc' => 'On if you want the controls (such as a play/pause button etc) hide automaticaly.',
                'default' => '1',
            ),

        ),
    ));

}

// require_once "option-page.php";
// require_once 'playlist-meta.php';

/**
 *
 * Field: password
 *
 * @since 1.0.0
 * @version 1.0.0
 *
 */
// if( ! class_exists( 'CSF_Field_password' ) ) {
//     class CSF_Field_password extends CSF_Fields {
  
//       public function __construct( $field, $value = '', $unique = '', $where = '', $parent = '' ) {
//         parent::__construct( $field, $value, $unique, $where, $parent );
//       }
  
//       public function render() {
//         echo $this->field_before();
//         echo '<input type="password" name="'. $this->field_name() .'" value="'. $this->value .'"'. $this->field_attributes() .' />';
//         echo '<button type="button" class="button button-secondary wp-hide-pw hide-if-no-js h5vp_show_password" data-toggle="0" aria-label="Show password"><span class="dashicons dashicons-visibility" aria-hidden="true"></span></button>';
//         echo $this->field_after();
  
//       }
  
//     }
//   }